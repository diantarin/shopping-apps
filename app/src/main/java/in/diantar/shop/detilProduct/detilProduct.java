package in.diantar.shop.detilProduct;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import in.diantar.shop.R;
import in.diantar.shop.viewKeranjang.ItemCart;

public class detilProduct extends Fragment implements View.OnClickListener {

    List<GetProductDetilAdapter> GetProductDetilAdapter;
    Context mContext;
    private RecyclerView recyclerView;
    private PopupWindow pwindo;

    RecyclerView.Adapter recyclerViewProdukDetiladapter;

    String JSON_PRODUCT_NAME = "name";
    String JSON_PRODUCT_IMG = "image";
    String JSON_PRODUCT_ID = "productid";
    String JSON_PRODUCT_PRICE = "harga";
    String JSON_PRODUCT_SHORT_DESC = "desc";

    JsonArrayRequest jsonArrayRequest;

    RequestQueue requestQueue;

    private TextView countCart, judul;
    private ExpandableRelativeLayout expandableLayout1;
    private ImageButton backto;

    int angkaQty = 1;

    //add to cart
    public static final String ADD_TO_CART_URL = "http://bungakasih.id:8080/webservices/simpan/index.jsp?q=KERANJANG";
    public static final String CEK_CART_URL = "http://bungakasih.id:8080/webservices/getdata.jsp?q=JUMLAHKERANJANG&device_id=";

    //  public static final String REGISTER_URL = "http://logvaksin-abar.com/android/addtocart.php";
    public static final String KEY_PRODUCT_ID = "product_id";
    public static final String KEY_PRODUCT_PRICE = "product_price";
    public static final String KEY_DEVICE_ID = "device_id";
    public static final String KEY_NAME = "nama";

    //listview cart item
    private ProgressDialog dialog;
    private List<ItemCart> array = new ArrayList<ItemCart>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments

        View view = inflater.inflate(R.layout.main_detil_product, container, false);//Inflate Layout
        final TextView textIdpro = (TextView) view.findViewById(R.id.idpro);//Find textview Id
        final TextView hprod = (TextView) view.findViewById(R.id.hargaInv);
        final TextView namaprod = (TextView) view.findViewById(R.id.namaInv);


        String productId = getArguments().getString("Product_ID");
        String hargaProd = getArguments().getString("Product_PRICE");
        String namaProd = getArguments().getString("Product_NAME");

        textIdpro.setText(productId);
        hprod.setText(hargaProd);
        namaprod.setText(namaProd);

        Button openSpek = (Button) view.findViewById(R.id.BtnSpesifikasi);
        expandableLayout1 = (ExpandableRelativeLayout) view.findViewById(R.id.spesifikasi);
        openSpek.setOnClickListener(this);

        countCart = (TextView) ((Activity) getContext()).findViewById(R.id.countCart);


        Button beli = (Button) view.findViewById(R.id.pesan);
        beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(getContext(), "open", Toast.LENGTH_SHORT).show();
                // LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);


                final String productId = textIdpro.getText().toString().trim();
                final String product_price = hprod.getText().toString().trim();
                final String nama = namaprod.getText().toString().trim();
                final String device_id = Settings.Secure.getString(getContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);

                addToCart(productId, product_price, device_id, nama, view);

            }
        });
        return view;//return view

    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }


    private void jumlahItem(final String devId) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(CEK_CART_URL + devId,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //parsing json
                        if (response.length() == 0) {
                            //Toasty.info(getApplication(), "Keranjang Belanja Anda Kosong..."+response, Toast.LENGTH_SHORT).show();
                            countCart.setVisibility(View.INVISIBLE);
                        } else {
                            for (int x = 0; x < response.length(); x++) {
                                try {
                                    JSONObject obj = response.getJSONObject(x);
                                    // Toast.makeText(MainActivity.this, obj.getString("jml"), Toast.LENGTH_SHORT).show();
                                    countCart.setVisibility(View.VISIBLE);
                                    countCart.setText(obj.getString("jml"));
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ServerError) {
                    message = "Server tidak ditemukan...";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing data Error...";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut...";
                }
                Toasty.error(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);
    }


    public void addToCart(final String productId, final String product_price, final String device_id, final String nama, View view) {
        final ProgressDialog loading = ProgressDialog.show(getContext(), "Please Wait...", "Memasukan ke Keranjang Belanja...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ADD_TO_CART_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        response = response.trim();
                        jumlahItem(device_id);
                        Toasty.success(getContext(), response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_PRODUCT_ID, productId);
                params.put(KEY_PRODUCT_PRICE, product_price);
                params.put(KEY_DEVICE_ID, device_id);
                params.put(KEY_NAME, nama);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onClick(View v) {
        expandableLayout1.toggle();
    }


    public static detilProduct newInstance() {
        detilProduct fragment = new detilProduct();
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        judul = (TextView) getActivity().findViewById(R.id.titleToolbar);
        backto = (ImageButton) getActivity().findViewById(R.id.backto);

        backto.setVisibility(View.VISIBLE);
        getActivity().setTitle("");
        judul.setText("Detail Info");

        backto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                judul.setText("Product");
                getFragmentManager().popBackStack();
            }
        });

        GetProductDetilAdapter = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.rvProductDetil);
        recyclerView.setHasFixedSize(true);

        TextView idProduct = (TextView) view.findViewById(R.id.idpro);

        String productId = idProduct.getText().toString();

        recyclerView.setLayoutManager(new GridLayoutManager(this.getActivity(), 1));
        getDetilProduct(productId);


    }

    public void getDetilProduct(String productId) {
        final ProgressDialog loading = ProgressDialog.show(this.getActivity(), "Please Wait...", "Fetching product...", false, false);

        jsonArrayRequest = new JsonArrayRequest("http://bungakasih.id:8080/webservices/getdata.jsp?q=CARIPRODUCT&i=" + productId,
                //  jsonArrayRequest = new JsonArrayRequest("http://logvaksin-abar.com/android/detilproduk.php?i="+productId,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        loading.dismiss();
                        JSONParser(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        requestQueue = Volley.newRequestQueue(this.getContext());

        requestQueue.add(jsonArrayRequest);
    }

    public void JSONParser(JSONArray array) {

        for (int i = 0; i < array.length(); i++) {

            GetProductDetilAdapter getProductDetilAdapter = new GetProductDetilAdapter();

            JSONObject json = null;
            try {

                json = array.getJSONObject(i);
                getProductDetilAdapter.setProductName(json.getString(JSON_PRODUCT_NAME));
                getProductDetilAdapter.setProductImage(json.getString(JSON_PRODUCT_IMG));
                getProductDetilAdapter.setProductId(json.getString(JSON_PRODUCT_ID));
                getProductDetilAdapter.setProductPrice(json.getString(JSON_PRODUCT_PRICE));
                getProductDetilAdapter.setProductDesc(json.getString(JSON_PRODUCT_SHORT_DESC));

            } catch (JSONException e) {

                e.printStackTrace();
            }
            GetProductDetilAdapter.add(getProductDetilAdapter);
        }

        recyclerViewProdukDetiladapter = new RVProductDetilAdapter(GetProductDetilAdapter, getContext());

        recyclerView.setAdapter(recyclerViewProdukDetiladapter);

    }

}
