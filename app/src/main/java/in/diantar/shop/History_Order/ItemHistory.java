package in.diantar.shop.History_Order;

/**
 * Created by MasToro on 03/04/2017.
 */

public class ItemHistory {

    public String no, id, nomor_order, total, tgl, state;

    public void setNo(String no) {
        this.no = no;
    }

    public String getNo() {
        return no;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNomor_order(String nomor_order) {
        this.nomor_order = nomor_order;
    }

    public String getNomor_order() {
        return nomor_order;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public ItemHistory() {
    }
}

