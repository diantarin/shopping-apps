package in.diantar.shop.History_Order;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import in.diantar.shop.Config;
import in.diantar.shop.R;
import in.diantar.shop.viewKeranjang.ItemCart;
import in.diantar.shop.viewKeranjang.cartController;

/**
 * Created by MasToro on 06/05/2017.
 */

public class historyAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Activity activity;
    private List<ItemHistory> itemHistori;
    private TextView clearHistory, xclose_popup;
    private List<ItemCart> itemCart;
    private List<ItemCart> array = new ArrayList<ItemCart>();
    NumberFormat rupiahFormat;
    String Rupiah = "Rp.";
    RelativeLayout detilHistory;
    PopupWindow openHistory;
    ProgressDialog dialog;
    detilHistoryAdapter detilhistoryAdapter;

    TextView idhistory, no_order, tgl_order, state, sumtotal;
    protected static TextView total;

    ImageLoader imageLoader = cartController.getmInstance().getmImageLoader();

    public historyAdapter(Activity activity, List<ItemHistory> itemHistori) {
        this.activity = activity;
        this.itemHistori = itemHistori;

    }

    @Override
    public int getCount() {
        return itemHistori.size();
    }

    @Override
    public Object getItem(int position) {
        return itemHistori.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void removeItem(int position) {
        itemHistori.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.history_order_layout, null);
        }
        if (imageLoader == null)
            imageLoader = cartController.getmInstance().getmImageLoader();

        final ItemHistory ihistor = itemHistori.get(position);
        idhistory = (TextView) convertView.findViewById(R.id.idHistory);
        no_order = (TextView) convertView.findViewById(R.id.no_order);
        tgl_order = (TextView) convertView.findViewById(R.id.tgl_order);
        state = (TextView) convertView.findViewById(R.id.state);
        total = (TextView) convertView.findViewById(R.id.totalBayar);

        idhistory.setText(ihistor.getId());
        no_order.setText(ihistor.getNomor_order());
        tgl_order.setText(ihistor.getTgl());

        rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        String rp_total = rupiahFormat.format(Double.parseDouble(ihistor.getTotal()));
        String hasil = Rupiah + " " + rp_total;
        total.setText(hasil);
        state.setText(ihistor.getState());

       /* clearHistory = (TextView) convertView.findViewById(R.id.clearHistory);
        clearHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String idOrder = itemHistori.get(position).getId();
                final String noOrder = itemHistori.get(position).getNomor_order();

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Konfirmasi");
                builder.setMessage("Riwayat Order dengan no. "+noOrder+" akan dihapus?");

                builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        removeItem(position);
                        dialog.dismiss();

                    }
                });
                builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });*/


        detilHistory = (RelativeLayout) convertView.findViewById(R.id.detilHistory);
        detilHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idorder = ihistor.getId();
                String subtotal = total.getText().toString();
                detilHistoryOrder(v, idorder);
            }
        });


        //  Toast.makeText(activity, ihistor.getNomor_order(), Toast.LENGTH_SHORT).show();
        return convertView;
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    private void detilHistoryOrder(View v, final String idorder) {
        try {

            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(activity.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.frame_history_order,
                    (ViewGroup) v.findViewById(R.id.popup_history));
            openHistory = new PopupWindow(activity);
            openHistory.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            openHistory.setWidth(layout.getMeasuredWidth());
            openHistory.setFocusable(true);
            openHistory.setOutsideTouchable(true);
            openHistory.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            openHistory.setContentView(layout);
            openHistory.showAtLocation(layout, Gravity.CENTER, 0, 0);


            //list item di keranjang belanja
            final ListView lview = (ListView) layout.findViewById(R.id.list_item_history);
            sumtotal = (TextView) layout.findViewById(R.id.sumtotal);


            final Button tutup = (Button) layout.findViewById(R.id.tutup);
            tutup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openHistory.dismiss();
                }
            });

            final Button orderLagi = (Button) layout.findViewById(R.id.quick_order);
            orderLagi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, "Order Lagi Bos..", Toast.LENGTH_SHORT).show();
                }
            });


            //adapter ListView Item Keranjang Belanja
            detilhistoryAdapter = new detilHistoryAdapter((Activity) activity, array);
            lview.setAdapter(detilhistoryAdapter);


            dialog = new ProgressDialog(activity);
            dialog.setMessage("please wait...");
            dialog.show();

            //Creat volley request obj
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Config.GET_DETIL_RIWAYAT + idorder,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            hideDialog();
                            //parsing json
                            array.clear();
                            if (response.length() == 0) {
                                Toasty.info(activity, response.toString().trim(), Toast.LENGTH_SHORT).show();
                            } else {
                                for (int x = 0; x < response.length(); x++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(x);

                                        ItemCart iriwayat = new ItemCart();
                                        iriwayat.setId(obj.getString("product_id"));
                                        iriwayat.setName(obj.getString("nama_barang"));
                                        iriwayat.setImage(obj.getString("image"));
                                        iriwayat.setPrice(obj.getString("harga_satuan"));
                                        iriwayat.setQuantity(obj.getString("qty"));
                                        iriwayat.setSubtotal(obj.getString("subtotal"));
                                        //add to array
                                        array.add(iriwayat);

                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    }
                                }
                                detilhistoryAdapter.notifyDataSetChanged();
                                hitungTotal();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    String message = null;
                    if (volleyError instanceof NetworkError) {
                        message = "Tidak ada koneksi Internet...";
                    } else if (volleyError instanceof ServerError) {
                        message = "Server tidak ditemukan...";
                    } else if (volleyError instanceof AuthFailureError) {
                        message = "Tidak ada koneksi Internet...";
                    } else if (volleyError instanceof ParseError) {
                        message = "Parsing data Error...";
                    } else if (volleyError instanceof TimeoutError) {
                        message = "Koneksi TimeOut...";
                    }
                    Toasty.error(activity, message, Toast.LENGTH_LONG).show();
                }
            });
            cartController.getmInstance().addToRequesQueue(jsonArrayRequest);

            //close popup pojok kanan atas
            xclose_popup = (TextView) layout.findViewById(R.id.close_popup);
            xclose_popup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lview.setAdapter(null);
                    openHistory.dismiss();

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void hitungTotal() {
        double total = 0;
        int count = detilhistoryAdapter.getCount();
        for (int i = 0; i < count; i++) {
            String dprice = array.get(i).getSubtotal();
            total += Double.parseDouble(dprice);

        }
        rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        String rp_total = rupiahFormat.format(total);
        String hasil = Rupiah + " " + rp_total;
        sumtotal.setText(hasil);

    }


}
