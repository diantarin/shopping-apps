package in.diantar.shop;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class CheckOut extends AppCompatActivity {

    private CardView cardjadwal;
    private Button submit;

    protected static TextView resTgl, resJam, antarin, TglText, JamText;
    private TextView mailCus, hpCus, aparCus, towerCus, lantaiCus, nomorCus, idApartemen, namaCus;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSkedul);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_avatar);
        }
        setTitle("checkout");
        TglText = (TextView) findViewById(R.id.TglKirim);
        JamText = (TextView) findViewById(R.id.JamKirim);
        resTgl = (TextView) findViewById(R.id.resTglKirim);
        resJam = (TextView) findViewById(R.id.resJamKirim);
        antarin = (TextView) findViewById(R.id.diantarin);


        idApartemen = (TextView) findViewById(R.id.idApartemen);
        namaCus = (TextView) findViewById(R.id.Header);
        mailCus = (TextView) findViewById(R.id.emailCustomer);
        hpCus = (TextView) findViewById(R.id.noHpCustomer);
        aparCus = (TextView) findViewById(R.id.ApartemenCustomer);
        towerCus = (TextView) findViewById(R.id.towerAp);
        lantaiCus = (TextView) findViewById(R.id.lantaiAp);
        nomorCus = (TextView) findViewById(R.id.nomorAp);

        submit = (Button) findViewById(R.id.submitConfirm);


        //get customer info
        final String device_id = Settings.Secure.getString(getApplication().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        getInfoUser(device_id);

        cardjadwal = (CardView) findViewById(R.id.cardSkedul);
        cardjadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTanggal();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String idA = idApartemen.getText().toString();
                String tgl = resTgl.getText().toString();
                String jam = resJam.getText().toString();
                String email = mailCus.getText().toString();

                if ((jam.length() != 0) && (tgl.length() != 0)) {

                    confirmBeli(device_id, tgl, jam, email, idA);
                    //Snackbar.make(v," Confirm ",Snackbar.LENGTH_LONG).show();

                } else {
                    Snackbar.make(v, "Anda belum menentukan Waktu Pengiriman", Snackbar.LENGTH_LONG).show();
                }

            }
        });
    }

    public void getTanggal() {
        DatePickerFragment mDatePicker = new DatePickerFragment();
        mDatePicker.show(getFragmentManager(), "Pilih Tanggal");
    }


    private void getInfoUser(final String devId) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Config.GET_USER_INFO + devId,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //parsing json
                        if (response.length() == 0) {
                            Toasty.info(getApplication(), response.toString(), Toast.LENGTH_SHORT).show();
                            //countCart.setVisibility(View.INVISIBLE);
                        } else {
                            for (int x = 0; x < response.length(); x++) {
                                try {
                                    JSONObject obj = response.getJSONObject(x);
                                    idApartemen.setText(obj.getString(Config.USER_APARTEMENT_ID));
                                    namaCus.setText(obj.getString(Config.USER_NAME));
                                    mailCus.setText(obj.getString(Config.USER_MAIL));
                                    hpCus.setText(obj.getString(Config.USER_PHONE_NUMBER));
                                    aparCus.setText(obj.getString(Config.USER_APARTEMENT_NAME));
                                    towerCus.setText("Tower " + obj.getString(Config.USER_APARTEMENT_TOWER).toUpperCase());
                                    lantaiCus.setText("Lantai " + obj.getString(Config.USER_APARTEMENT_LANTAI).toUpperCase());
                                    nomorCus.setText("No. " + obj.getString(Config.USER_APARTEMENT_NOMOR).toUpperCase());

                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ServerError) {
                    message = "Server tidak ditemukan...";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing data Error...";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut...";
                }
                Toasty.error(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Date today = new Date();
            final Calendar c = Calendar.getInstance();

            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

           /* c.setTime(today);
            c.add(Calendar.MONTH,1);
            long minDate = c.getTime().getTime();
            //return new DatePickerDialog(getActivity(), this, year, month, day);
            DatePickerDialog pickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            pickerDialog.getDatePicker().setMinDate(minDate);*/

            final DatePickerDialog datePickerDialog = new DatePickerDialog(
                    getActivity(), this,
                    year, month, day);
            DatePicker datePicker = datePickerDialog.getDatePicker();

            // c.add(Calendar.MONTH, +1);
            c.add(Calendar.DAY_OF_MONTH, +20);
            long oneMonthAhead = c.getTimeInMillis();
            datePicker.setMaxDate(oneMonthAhead);
            datePicker.setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();


            return datePickerDialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // displayCurrentTime.setText("Selected date: " + String.valueOf(year) + " - " + String.valueOf(month) + " - " + String.valueOf(day));
            //   Toast.makeText(getActivity(), "Selected date: " + String.valueOf(year) + " - "+ String.valueOf(month) + " - " + String.valueOf(day), Toast.LENGTH_SHORT).show();
            TglText.setVisibility(View.VISIBLE);
            resTgl.setText(String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year));

            getTime();
        }

        private void getTime() {
            TimePicker mTimePicker = new TimePicker();
            mTimePicker.show(getFragmentManager(), "Select time");
        }

        public static class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);

               /* c.add(Calendar.HOUR_OF_DAY,+5);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
                */

                final TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), this,
                        c.get(Calendar.HOUR_OF_DAY) - 3, c.get(Calendar.MINUTE), false);
                timePickerDialog.show();
                return timePickerDialog;
            }

            @Override
            public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
                JamText.setVisibility(View.VISIBLE);
                resJam.setText(String.valueOf(hourOfDay) + ":" + String.valueOf(minute));
                antarin.setText("Klik untuk merubah");
            }
        }
    }


    public void confirmBeli(final String devId, final String tanggal, final String jam, final String email, final String idApartemen) {
        final ProgressDialog loading = ProgressDialog.show(this, "Processing...", "please wait...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://bungakasih.id:8080/webservices/beli?device=" + devId + "&tgl=" + tanggal + "&jam=" + jam + "&email=" + email + "&toko=" + idApartemen,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Toasty.success(CheckOut.this, "Order Confirmed...", Toast.LENGTH_LONG).show();
                        //close activity dan back to mainActivity
                        finish();

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(activity,error.toString(),Toast.LENGTH_LONG).show();
                        Toasty.error(CheckOut.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Config.KEY_DEVICE_ID, devId);
                params.put(Config.KEY_CONFIRM_TANGGAL, tanggal);
                params.put(Config.KEY_CONFIRM_JAM, jam);
                params.put(Config.KEY_CONFIRM_EMAIL, email);
                params.put(Config.USER_APARTEMENT_ID, idApartemen);

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

}
