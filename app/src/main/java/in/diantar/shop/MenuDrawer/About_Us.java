package in.diantar.shop.MenuDrawer;

import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import in.diantar.shop.R;

/**
 * Created by Belal on 18/09/16.
 */


public class About_Us extends Fragment {
    private TextView judul;
    private ImageButton backto;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_aboutus, container, false);//Inflate Layout

        final TextView textView = (TextView) view.findViewById(R.id.textAbout);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String device_id = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
                Toast.makeText(getActivity(), "Test Click", Toast.LENGTH_SHORT).show();
                textView.setText(device_id);
            }
        });
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles

        judul = (TextView) getActivity().findViewById(R.id.titleToolbar);
        backto = (ImageButton) getActivity().findViewById(R.id.backto);

        backto.setVisibility(View.VISIBLE);
        getActivity().setTitle("");
        judul.setText("Tentang Kami");

        backto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backto.setVisibility(View.INVISIBLE);
                judul.setText("diantar.in");
                getFragmentManager().popBackStack();
            }
        });

    }
}
