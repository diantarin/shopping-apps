package in.diantar.shop.MenuDrawer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import in.diantar.shop.MainActivity;
import in.diantar.shop.R;
import in.diantar.shop.Rounded_Image.CircularNetworkImageView;

/**
 * Created by Belal on 18/09/16.
 */


public class Profile extends Fragment {

    private ImageButton backto;
    private CircularNetworkImageView avatarProfil;
    private TextView judul, nama, email, nomorhp, apartemen, tower, lantai, nomor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profilsaya, container, false);//Inflate Layout
        avatarProfil = (CircularNetworkImageView) view.findViewById(R.id.avatarProfile);

        nama = (TextView) view.findViewById(R.id.namaUser);
        email = (TextView) view.findViewById(R.id.mailUser);//Find textview Id
        apartemen = (TextView) view.findViewById(R.id.namaApart);
        tower = (TextView) view.findViewById(R.id.towerInfo);
        lantai = (TextView) view.findViewById(R.id.lantaiInfo);
        nomor = (TextView) view.findViewById(R.id.nomorInfo);

        String getNama = getArguments().getString("namaUsr");
        String getMail = getArguments().getString("emailUsr");
        String getApart = getArguments().getString("apartemen");
        String getTower = getArguments().getString("tower");
        String getLantai = getArguments().getString("lantai");
        String getNomor = getArguments().getString("nomor");

        nama.setText(getNama);
        email.setText(getMail);
        apartemen.setText(getApart);
        tower.setText(getTower);
        lantai.setText(getLantai);
        nomor.setText(getNomor);


        avatarProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAvatar();
            }
        });

        ((MainActivity) getActivity()).closeDrawer();

        // Toast.makeText(getContext(), pesan, Toast.LENGTH_SHORT).show();
        return view;//return view


    }

    public void changeAvatar() {

    }

    public static Profile newInstance() {
        Profile fragment = new Profile();
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        judul = (TextView) getActivity().findViewById(R.id.titleToolbar);
        backto = (ImageButton) getActivity().findViewById(R.id.backto);

        backto.setVisibility(View.VISIBLE);
        getActivity().setTitle("");
        judul.setText("Profil Saya");

        backto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backto.setVisibility(View.INVISIBLE);
                judul.setText("diantar.in");
                getFragmentManager().popBackStack();
            }
        });

    }


}
