package in.diantar.shop.MenuDrawer;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.diantar.shop.Config;
import in.diantar.shop.History_Order.ItemHistory;
import in.diantar.shop.History_Order.historyAdapter;
import in.diantar.shop.R;
import in.diantar.shop.viewKeranjang.cartController;

public class RiwayatOrder extends Fragment {

    private ProgressDialog dialog;
    private List<ItemHistory> itemHistory = new ArrayList<ItemHistory>();
    private historyAdapter histoAdapter;
    private ImageButton backto;
    protected static TextView judul;
    private Button kembali;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_riwayat_order, container, false);//Inflate Layout
        return view;//return view
    }

    public static RiwayatOrder newInstance() {
        RiwayatOrder fragment = new RiwayatOrder();
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        judul = (TextView) getActivity().findViewById(R.id.titleToolbar);
        kembali = (Button) getActivity().findViewById(R.id.kembali);
        backto = (ImageButton) getActivity().findViewById(R.id.backto);

        backto.setVisibility(View.VISIBLE);
        getActivity().setTitle("");
        judul.setText("Riwayat Order");

        backto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backto.setVisibility(View.INVISIBLE);
                judul.setText("diantar.in");
                getFragmentManager().popBackStack();
            }
        });

        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backto.setVisibility(View.INVISIBLE);
                judul.setText("diantar.in");
                getFragmentManager().popBackStack();
            }
        });

        final LinearLayout cartFooter = (LinearLayout) getActivity().findViewById(R.id.cartFooter);
        cartFooter.setVisibility(View.INVISIBLE);

        final ListView historyListView = (ListView) view.findViewById(R.id.list_history);
        histoAdapter = new historyAdapter(getActivity(), itemHistory);
        historyListView.setAdapter(histoAdapter);

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("please wait...");
        dialog.show();

        //Creat volley request obj
        final String device_id = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Config.GET_RIWAYAT_ORDER + device_id, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                hideDialog();
                itemHistory.clear();
                //parsing json
                if (response.length() == 0) {

                    cartFooter.setVisibility(View.INVISIBLE);
                    RelativeLayout historyIsEmpty = (RelativeLayout) getActivity().findViewById(R.id.HistoryEmpty);
                    historyListView.setVisibility(View.INVISIBLE);
                    historyIsEmpty.setVisibility(View.VISIBLE);
                    // Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_SHORT).show();

                } else {
                    LinearLayout cartFooter = (LinearLayout) getActivity().findViewById(R.id.cartFooter);
                    RelativeLayout historyIsEmpty = (RelativeLayout) getActivity().findViewById(R.id.HistoryEmpty);
                    historyListView.setVisibility(View.VISIBLE);
                    historyIsEmpty.setVisibility(View.INVISIBLE);
                    for (int x = 0; x < response.length(); x++) {
                        try {
                            JSONObject obj = response.getJSONObject(x);
                            ItemHistory ihistory = new ItemHistory();
                            ihistory.setNo(obj.getString("no"));
                            ihistory.setId(obj.getString("id"));
                            ihistory.setNomor_order(obj.getString("nomor_order"));
                            ihistory.setTgl(obj.getString("tgl"));
                            ihistory.setTotal(obj.getString("total"));
                            ihistory.setState(obj.getString("state"));
                            //add to array

                            itemHistory.add(ihistory);

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                    histoAdapter.notifyDataSetChanged();
                    cartFooter.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        cartController.getmInstance().addToRequesQueue(jsonArrayRequest);


    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

}
