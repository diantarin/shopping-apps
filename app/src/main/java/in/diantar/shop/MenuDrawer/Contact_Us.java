package in.diantar.shop.MenuDrawer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import in.diantar.shop.R;

/**
 * Created by Belal on 18/09/16.
 */


public class Contact_Us extends Fragment {
    private ImageButton backto;
    private TextView judul;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.fragment_contactus, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        judul = (TextView) getActivity().findViewById(R.id.titleToolbar);

        backto = (ImageButton) getActivity().findViewById(R.id.backto);
        backto.setVisibility(View.VISIBLE);
        backto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backto.setVisibility(View.INVISIBLE);
                judul.setText("diantar.in");
                getFragmentManager().popBackStack();
            }
        });
        getActivity().setTitle("");
        judul.setText("Hubungi Kami");
    }
}
