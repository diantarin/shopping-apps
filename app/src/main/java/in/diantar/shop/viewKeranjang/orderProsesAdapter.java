package in.diantar.shop.viewKeranjang;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import in.diantar.shop.R;
import me.himanshusoni.quantityview.QuantityView;

import static java.lang.Integer.parseInt;

/**
 * Created by MasToro on 03/04/2017.
 */

public class orderProsesAdapter extends BaseAdapter {

    public static final String REMOVE_URL = "http://bungakasih.id:8080/webservices/simpan/index.jsp?q=DELETEKERANJANG&respon=JUMLAHKERANJANG";
    public static final String KEY_PRODUCT_ID = "product_id";
    public static final String KEY_DEVICE_ID = "device_id";
    public static final String CEK_CART_URL = "http://bungakasih.id:8080/webservices/getdata.jsp?q=JUMLAHKERANJANG&device_id=";

    private LayoutInflater inflater;
    private Activity activity;
    private List<ItemCart> itemCart;


    private TextView idListCartItem, price, subtot, countCart;
    private QuantityView qtyView;

    RequestQueue requestQueue;
    ImageLoader imageLoader = cartController.getmInstance().getmImageLoader();
    String Rupiah = "Rp.";
    NumberFormat rupiahFormat;

    public orderProsesAdapter(Activity activity, List<ItemCart> itemCart) {
        this.activity = activity;
        this.itemCart = itemCart;
    }


    @Override
    public int getCount() {
        return itemCart.size();
    }

    @Override
    public Object getItem(int position) {
        return itemCart.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void removeItem(int position) {
        itemCart.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.order_layout, null);
        }
        if (imageLoader == null)
            imageLoader = cartController.getmInstance().getmImageLoader();

        NetworkImageView imageView = (NetworkImageView) convertView.findViewById(R.id.image_view);
        TextView title = (TextView) convertView.findViewById(R.id.cartProdName);
        //  imageView.setRadius(15);
        imageView.setScaleX(1);

        idListCartItem = (TextView) convertView.findViewById(R.id.idListItem);
        qtyView = (QuantityView) convertView.findViewById(R.id.jqty);
        price = (TextView) convertView.findViewById(R.id.priceProduct);
        subtot = (TextView) convertView.findViewById(R.id.subTotal);
        final TextView stot = (TextView) convertView.findViewById(R.id.subTotal);

        countCart = (TextView) activity.findViewById(R.id.countCart);

        qtyView.setMaxQuantity(100);
        qtyView.setMinQuantity(1);
        qtyView.setOnQuantityChangeListener(new QuantityView.OnQuantityChangeListener() {
            @Override
            public void onQuantityChanged(int oldQuantity, int newQuantity, boolean programmatically) {


                ItemCart item = itemCart.get(position);
                String dprice = item.getPrice();
                double hitung = newQuantity * Double.parseDouble(dprice);
                // getTotal(itemCart);
                String hasil = rupiahFormat.format(hitung);
                String sTotal = Rupiah + " " + hasil;
                // price.setText(String.valueOf(sTotal));
                // Toast.makeText(activity,subtot.getText(), Toast.LENGTH_SHORT).show();
                stot.setText(String.valueOf(sTotal));

            }

            @Override
            public void onLimitReached() {

            }
        });


        //getting data for row
        final ItemCart icart = itemCart.get(position);
        imageView.setImageUrl(icart.getImage(), imageLoader);

        rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        String rupiah = rupiahFormat.format(Double.parseDouble(icart.getPrice()));
        String sbtot = rupiahFormat.format(Double.parseDouble(icart.getSubtotal()));
        String Result = Rupiah + " " + rupiah;
        String Stot = Rupiah + " " + sbtot;

        // title.setText(itemCart.getName());
        title.setText(icart.getName());
        idListCartItem.setText(icart.getId());
        price.setText(Result);
        subtot.setText(Stot);
        qtyView.setQuantity(parseInt(icart.getQuantity()));
        idListCartItem.setVisibility(View.VISIBLE);


        //trash produk di keranjang
      /*  xtrash = (ImageButton)convertView.findViewById(R.id.delProdukOnCart);
        xtrash.setTag(position);
        xtrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                //get id Produk
                final String idProduk = icart.getId();
                final String device_id = Settings.Secure.getString(activity.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                // Toast.makeText(activity, idProduk, Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Konfirmasi");
                builder.setMessage("Item Produk akan dihapus dari Keranjang Belanja?");

                builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        removeItem(position);
                        delete(idProduk,device_id);
                        dialog.dismiss();

                    }
                });
                builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });*/
        return convertView;
    }

    private void jumlahItem(final String devId) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(CEK_CART_URL + devId,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //parsing json
                        if (response.length() == 0) {
                            //Toasty.info(getApplication(), "Keranjang Belanja Anda Kosong..."+response, Toast.LENGTH_SHORT).show();
                            countCart.setVisibility(View.INVISIBLE);
                        } else {
                            for (int x = 0; x < response.length(); x++) {
                                try {
                                    JSONObject obj = response.getJSONObject(x);
                                    // Toast.makeText(MainActivity.this, obj.getString("jml"), Toast.LENGTH_SHORT).show();
                                    countCart.setVisibility(View.VISIBLE);
                                    countCart.setText(obj.getString("jml"));
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ServerError) {
                    message = "Server tidak ditemukan...";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing data Error...";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut...";
                }
                Toasty.error(activity, message, Toast.LENGTH_LONG).show();
            }
        });
        requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(jsonArrayRequest);
    }

    private void deleteItem(final String idx, final String devId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REMOVE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        response = response.trim();
                        if (response.equals("kosong")) {
                            //Toasty.info(activity,"Kosong",Toast.LENGTH_LONG).show();
                            Toasty.success(activity, "Item Order telah dihapus", Toast.LENGTH_LONG).show();
                        } else {
                            Toasty.success(activity, "Item Order telah dihapus", Toast.LENGTH_LONG).show();

                        }
                        jumlahItem(devId);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_PRODUCT_ID, idx);
                params.put(KEY_DEVICE_ID, devId);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }
}
