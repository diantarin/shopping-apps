package in.diantar.shop.viewKeranjang;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import in.diantar.shop.Config;
import in.diantar.shop.R;
import me.himanshusoni.quantityview.QuantityView;

/**
 * Created by MasToro on 03/04/2017.
 */

public class cartAdapter extends BaseAdapter {
    public static final String REMOVE_URL = "http://bungakasih.id:8080/webservices/simpan/index.jsp?q=DELETEKERANJANG&respon=JUMLAHKERANJANG";
    public static final String CEK_CART_URL = "http://bungakasih.id:8080/webservices/getdata.jsp?q=JUMLAHKERANJANG&device_id=";
    public static final String GET_CART_ITEM = "http://bungakasih.id:8080/webservices/getdata.jsp?q=ITEMKERANJANG&d=";

    public static final String KEY_PRODUCT_ID = "product_id";
    public static final String KEY_DEVICE_ID = "device_id";
    public static final String KEY_QTY = "quantity";
    public static final String KEY_SUBTOTAL = "subtotal";
    public static final String KEY_PRODUCT_PRICE = "product_price";

    private Context context;
    private LayoutInflater inflater;
    private Activity activity;
    private List<ItemCart> itemCart;
    private List<ItemCart> array = new ArrayList<ItemCart>();
    editProsesAdapter editProses;
    private TextView idListCartItem, price, subtot, qtyBuy, xtrash, xedit, countCart, xclose_edit, stotal;
    private Button blonjosik;
    private PopupWindow editPopup;
    ProgressDialog dialog;
    String Rupiah = "Rp.";
    NumberFormat rupiahFormat;
    RequestQueue requestQueue;

    ImageLoader imageLoader = cartController.getmInstance().getmImageLoader();
    private Fragment fragment;

    public cartAdapter(Activity activity, List<ItemCart> itemCart, Fragment fragment) {
        this.activity = activity;
        this.itemCart = itemCart;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return itemCart.size();
    }

    @Override
    public Object getItem(int position) {
        return itemCart.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void removeItem(int position) {
        itemCart.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_cart_layout, null);
        }
        if (imageLoader == null)
            imageLoader = cartController.getmInstance().getmImageLoader();

        NetworkImageView imageView = (NetworkImageView) convertView.findViewById(R.id.image_view);
        TextView title = (TextView) convertView.findViewById(R.id.cartProdName);
        // imageView.setRadius(15);

        idListCartItem = (TextView) convertView.findViewById(R.id.idListItem);

        qtyBuy = (TextView) convertView.findViewById(R.id.qtyProduk);
        price = (TextView) convertView.findViewById(R.id.priceProduct);
        subtot = (TextView) convertView.findViewById(R.id.subTotal);


        //getting data for row
        final ItemCart icart = itemCart.get(position);
        imageView.setImageUrl(icart.getImage(), imageLoader);

        rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        String rupiah = rupiahFormat.format(Double.parseDouble(icart.getPrice()));
        String sbtot = rupiahFormat.format(Double.parseDouble(icart.getSubtotal()));
        String gprice = Rupiah + " " + rupiah;
        String gstot = Rupiah + " " + sbtot;
        // title.setText(itemCart.getName());
        title.setText(icart.getName());
        idListCartItem.setText(icart.getId());
        price.setText(gprice);
        subtot.setText(gstot);
        qtyBuy.setText(icart.getQuantity());

        //qtyView.setQuantity(parseInt(icart.getQuantity()));

        idListCartItem.setVisibility(View.INVISIBLE);
        countCart = (TextView) activity.findViewById(R.id.countCart);
        stotal = (TextView) activity.findViewById(R.id.sumtotal);


        //edit item Produk from Cart
        xedit = (TextView) convertView.findViewById(R.id.editProduk);
        xedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String idProduk = icart.getId();
                final String device_id = Settings.Secure.getString(activity.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                //Toast.makeText(activity, idProduk, Toast.LENGTH_SHORT).show();
                showEditPopup(view, idProduk, device_id);

            }
        });


        //trash produk di keranjang
        xtrash = (TextView) convertView.findViewById(R.id.delProdukOnCart);
        xtrash.setTag(position);
        xtrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                //get id Produk
                final String idProduk = icart.getId();
                final String device_id = Settings.Secure.getString(activity.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                // Toast.makeText(activity, idProduk, Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Konfirmasi");
                builder.setMessage("Item Produk akan dihapus dari Keranjang Belanja?");

                builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        removeItem(position);
                        deleteItem(idProduk, device_id);
                        dialog.dismiss();
                        int j = getCount();
                        double total = 0;
                        for (int x = 0; x < j; x++) {
                            total += Double.parseDouble(itemCart.get(x).getSubtotal());
                        }
                        /// Toast.makeText(activity, String.valueOf(total), Toast.LENGTH_SHORT).show();
                        String rp_total = rupiahFormat.format(total);
                        String hasil = Rupiah + " " + rp_total;
                        stotal.setText(hasil);


                    }
                });
                builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        return convertView;
    }

    private void updateItemCart(final String devId, final String product_id, final String quantity, final double product_price, final double subtotal) {
        final ProgressDialog loading = ProgressDialog.show(activity, "Mohon tunggu...", "updating data...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_CART_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        response = response.trim();
                        Toasty.success(activity, response, Toast.LENGTH_LONG).show();
                        jumlahItem(devId);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_PRODUCT_ID, product_id);
                params.put(KEY_DEVICE_ID, devId);
                params.put(KEY_QTY, quantity);
                params.put(KEY_SUBTOTAL, String.valueOf(subtotal));
                params.put(KEY_PRODUCT_PRICE, String.valueOf(product_price));
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

    private void showEditPopup(View v, final String idx, final String device_id) {
        try {

            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(activity.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.frame_edit_popup,
                    (ViewGroup) v.findViewById(R.id.popup_edit_container));
            editPopup = new PopupWindow(activity);
            editPopup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            editPopup.setWidth(layout.getMeasuredWidth());
            editPopup.setFocusable(true);
            editPopup.setOutsideTouchable(true);
            editPopup.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            editPopup.setContentView(layout);
            editPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);


            //list item di keranjang belanja
            final ListView lview = (ListView) layout.findViewById(R.id.list_item);
            // idItem = (TextView)layout.findViewById(R.id.idListItem);

            final Button batalEdit = (Button) layout.findViewById(R.id.batal);
            final Button updateItem = (Button) layout.findViewById(R.id.updateItem);
            updateItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String device_id = Settings.Secure.getString(activity.getContentResolver(),
                            Settings.Secure.ANDROID_ID);

                    TextView prodID = (TextView) layout.findViewById(R.id.idListItem);
                    TextView hargaProduk = (TextView) layout.findViewById(R.id.hargaProduk);
                    TextView totalHarga = (TextView) layout.findViewById(R.id.totalHarga);
                    QuantityView jmQty = (QuantityView) layout.findViewById(R.id.quantity_view);


                    String xid = prodID.getText().toString();
                    double xharga = Double.parseDouble(hargaProduk.getText().toString());
                    double xtotal = Double.parseDouble(totalHarga.getText().toString());
                    String xqty = String.valueOf(jmQty.getQuantity());

                    updateItemCart(device_id, xid, xqty, xharga, xtotal);

                }
            });

            batalEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editPopup.dismiss();
                }
            });

            //adapter ListView Item Keranjang Belanja
            editProses = new editProsesAdapter((Activity) activity, array);
            lview.setAdapter(editProses);


            dialog = new ProgressDialog(activity);
            dialog.setMessage("mohon tunggu...");
            dialog.show();

            //Creat volley request obj
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(GET_CART_ITEM + device_id + "&id=" + idx,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            hideDialog();
                            //parsing json
                            array.clear();
                            if (response.length() == 0) {
                                Toasty.info(activity, "Keranjang Belanja Anda Kosong...", Toast.LENGTH_SHORT).show();
                            } else {
                                for (int x = 0; x < response.length(); x++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(x);

                                        ItemCart icart = new ItemCart();
                                        icart.setId(obj.getString("product_id"));
                                        icart.setName(obj.getString("nama"));
                                        icart.setImage(obj.getString("image"));
                                        icart.setPrice(obj.getString("product_price"));
                                        icart.setSubtotal(obj.getString("subtotal"));
                                        icart.setQuantity(obj.getString("quantity"));
                                        //add to array
                                        array.add(icart);

                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    }
                                }
                                editProses.notifyDataSetChanged();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    String message = null;
                    if (volleyError instanceof NetworkError) {
                        message = "Tidak ada koneksi Internet...";
                    } else if (volleyError instanceof ServerError) {
                        message = "Server tidak ditemukan...";
                    } else if (volleyError instanceof AuthFailureError) {
                        message = "Tidak ada koneksi Internet...";
                    } else if (volleyError instanceof ParseError) {
                        message = "Parsing data Error...";
                    } else if (volleyError instanceof TimeoutError) {
                        message = "Connection TimeOut...";
                    }
                    Toasty.error(activity, message, Toast.LENGTH_LONG).show();
                }
            });
            cartController.getmInstance().addToRequesQueue(jsonArrayRequest);

            //close popup pojok kanan atas
            xclose_edit = (TextView) layout.findViewById(R.id.close_edit);
            xclose_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lview.setAdapter(null);
                    editPopup.dismiss();

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }


    private void jumlahItem(final String devId) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(CEK_CART_URL + devId,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //parsing json
                        if (response.length() == 0) {
                            //Toasty.info(getApplication(), "Keranjang Belanja Anda Kosong..."+response, Toast.LENGTH_SHORT).show();
                            countCart.setVisibility(View.INVISIBLE);
                        } else {
                            for (int x = 0; x < response.length(); x++) {
                                try {
                                    JSONObject obj = response.getJSONObject(x);
                                    Toast.makeText(activity, obj.getString("jml"), Toast.LENGTH_SHORT).show();
                                    countCart.setVisibility(View.VISIBLE);
                                    countCart.setText(obj.getString("jml"));
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ServerError) {
                    message = "Server tidak ditemukan...";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing data Error...";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut...";
                }
                Toasty.error(activity, message, Toast.LENGTH_LONG).show();
            }
        });
        requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(jsonArrayRequest);
    }


    private void deleteItem(final String idx, final String devId) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REMOVE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //     cekCartItem(devId);
                        response = response.trim();
                        if (response.equals("kosong")) {

                            LinearLayout footers = (LinearLayout) activity.findViewById(R.id.cartFooter);
                            LinearLayout subTotalFooter = (LinearLayout) activity.findViewById(R.id.subTotLayout);
                            footers.setVisibility(View.INVISIBLE);
                            subTotalFooter.setVisibility(View.INVISIBLE);

                            RelativeLayout emptyCart = (RelativeLayout) activity.findViewById(R.id.layoutEmptyCart);
                            emptyCart.setVisibility(View.VISIBLE);

                            final ListView lview = (ListView) activity.findViewById(R.id.list_item);
                            lview.setVisibility(View.INVISIBLE);

                        } else {
                            // Toast.makeText(activity,response,Toast.LENGTH_LONG).show();
                            Toasty.success(activity, "Item barang dihapus", Toast.LENGTH_LONG).show();
                        }
                        final String device_id = Settings.Secure.getString(activity.getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        jumlahItem(device_id);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(activity,error.toString(),Toast.LENGTH_LONG).show();
                        Toasty.error(activity, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_PRODUCT_ID, idx);
                params.put(KEY_DEVICE_ID, devId);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }


}
