package in.diantar.shop;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.facebook.CallbackManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import es.dmoral.toasty.Toasty;
import in.diantar.shop.AllCategory.GetDataTerlarisAdapter;
import in.diantar.shop.AllCategory.RVCategoryTerlarisAdapter;
import in.diantar.shop.CartFragment.CartFragment;
import in.diantar.shop.CategoryTerlaris.GetDataAdapter;
import in.diantar.shop.CategoryTerlaris.RVCategoryAdapter;
import in.diantar.shop.MenuDrawer.About_Us;
import in.diantar.shop.MenuDrawer.Contact_Us;
import in.diantar.shop.MenuDrawer.Profile;
import in.diantar.shop.MenuDrawer.RiwayatOrder;
import in.diantar.shop.viewKeranjang.cartController;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    List<GetDataAdapter> GetDataAdapter;
    List<GetDataTerlarisAdapter> GetDataTerlarisAdapter;
    private ExpandListAdapter ExpAdapter;
    private RecyclerView recyclerView, rvTerlaris;
    private ImageView banner, imgProfile;
    private ImageButton backto;
    private RecyclerView.Adapter recyclerViewadapter, rvAdapterTerlaris;
    private RelativeLayout terlaris, terbaru, layout, shopCart;
    private LayoutInflater inflater;
    private LinearLayout linear, profileNavDraw;
    private TextView judul, err_MSG, countCart, namaUser, emailUser, hpUser, namaApartement, towerApartement, lantaiApartement, nomorApartement;
    private CallbackManager callbackManager;


    //public static final String GET_TERLARIS_URL = "http://bungakasih.id:8080/webservices/getdata.jsp?q=KATEGORI";
    public static final String GET_TERLARIS_URL = "http://logvaksin-abar.com/android/terlaris.php";

    public static final String GET_CATEGORY_URL = "http://bungakasih.id:8080/webservices/getdata.jsp?q=KATEGORI";
    public static final String CEK_CART_URL = "http://bungakasih.id:8080/webservices/getdata.jsp?q=JUMLAHKERANJANG&device_id=";
    public static final String GET_ALL_ITEM_CART = "http://bungakasih.id:8080/webservices/getdata.jsp?q=KERANJANG&d=";


    //mapping objek terlaris
    String TERLARIS_PRODUCT_NAME = "name";
    String TERLARIS_PRODUCT_IMG = "image";
    String TERLARIS_PRODUCT_ID = "product_id";

    //mapping objek semua kategori
    String CATEGORY_PRODUCT_NAME = "name";
    String CATEGORY_PRODUCT_IMG = "image";
    String CATEGORY_PRODUCT_ID = "id";

    public static final String KEY_PRODUCT_ID = "productId";
    public static final String KEY_DEVICE_ID = "device_id";

    JsonArrayRequest jsonArrayRequest;

    RequestQueue requestQueue;
    SwipeRefreshLayout swipe;
    Intent inten;

    DrawerLayout drawer;
    NavigationView navigationView;
    // ExpandableListView ExpandList;
    FloatingGroupExpandableListView ExpandList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Expandable List View Di Navigation Drawer
        ExpandList = (FloatingGroupExpandableListView) findViewById(R.id.exp_list);
        makejsonobjreqExpandable();


        ExpandList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                return false;
            }
        });

        backto = (ImageButton) findViewById(R.id.backto);
        judul = (TextView) findViewById(R.id.titleToolbar);
        backto.setVisibility(View.INVISIBLE);
        judul.setText("diantar.in");
        judul.setGravity(View.TEXT_ALIGNMENT_TEXT_START);

        //All Kategori
        GetDataAdapter = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.rvCategory2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));

        //Terlaris
       /* GetDataTerlarisAdapter = new ArrayList<>();
        rvTerlaris = (RecyclerView)findViewById(R.id.rvCategory);
        rvTerlaris.setHasFixedSize(true);
        rvTerlaris.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false));
        rvTerlaris.setScrollbarFadingEnabled(true);
*/

        //swiperRefresh Layout
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        int top_to_padding = 300;
        swipe.setProgressViewOffset(false, 0, top_to_padding);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                //getProductTerlaris();
                getAllCategory();
            }
        });

        //drawerLayout
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        //get objek item on navdraw
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);

        profileNavDraw = (LinearLayout) header.findViewById(R.id.profilNavDraw);
        namaUser = (TextView) header.findViewById(R.id.namaUser);
        emailUser = (TextView) header.findViewById(R.id.emailUser);
        namaApartement = (TextView) header.findViewById(R.id.namaApartement);
        towerApartement = (TextView) header.findViewById(R.id.tower);
        lantaiApartement = (TextView) header.findViewById(R.id.lantai);
        nomorApartement = (TextView) header.findViewById(R.id.nomor);

        navigationView.setNavigationItemSelectedListener(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.show();
        }


        profileNavDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = Profile.newInstance();
                Bundle bundle = new Bundle();
                String nama = namaUser.getText().toString();
                String mail = emailUser.getText().toString();
                String apartemen = namaApartement.getText().toString();
                String towerT = towerApartement.getText().toString();
                String lantaiT = lantaiApartement.getText().toString();
                String nomorT = nomorApartement.getText().toString();
                bundle.putString("namaUsr", nama);
                bundle.putString("emailUsr", mail);
                bundle.putString("apartemen", apartemen);
                bundle.putString("tower", towerT);
                bundle.putString("lantai", lantaiT);
                bundle.putString("nomor", nomorT);
                fragment.setArguments(bundle);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, fragment, "TAG_PROF_FRAGMENT");
                ft.addToBackStack(null);
                ft.commit();
            }
        });


        final String device_id = Settings.Secure.getString(getApplication().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        banner = (ImageView) findViewById(R.id.imageView);
        banner.setVisibility(View.INVISIBLE);

        //   terlaris = (RelativeLayout) findViewById(R.id.topHeader);
        terbaru = (RelativeLayout) findViewById(R.id.topHeader2);
//        terlaris.setVisibility(View.INVISIBLE);
        terbaru.setVisibility(View.INVISIBLE);

        inflater = LayoutInflater.from(getBaseContext());
        layout = (RelativeLayout) inflater.inflate(R.layout.layout_warning, null, false);
        err_MSG = (TextView) layout.findViewById(R.id.warningMsg);
        linear = (LinearLayout) findViewById(R.id.main);

        countCart = (TextView) findViewById(R.id.countCart);


        //icon keranjang di toolbar atas
        shopCart = (RelativeLayout) findViewById(R.id.counterValuePanel);
        shopCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String device_id = Settings.Secure.getString(getApplication().getContentResolver(),
                        Settings.Secure.ANDROID_ID);

              /*  Intent inten = new Intent(MainActivity.this, KeranjangBelanja.class);
                inten.putExtra("device_id",device_id);
                startActivityForResult(inten,1);
                */

                Fragment cartFragment = new CartFragment();
                Bundle bundle = new Bundle();
                bundle.putString("device_id", device_id);
                cartFragment.setArguments(bundle);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, cartFragment, "TAG_CART");
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        //   getProductTerlaris();
        getAllCategory();
        getInfoUser(device_id);

    }

  /*  public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String strEditText = data.getStringExtra("parameter");
                Toast.makeText(this, strEditText, Toast.LENGTH_SHORT).show();

            }
        }
    }*/

    public void closeDrawer() {
        drawer.closeDrawer(Gravity.LEFT);
    }

    private void makejsonobjreqExpandable() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, Config.GET_EXPAND_LIST_URL,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ArrayList<Group> list = new ArrayList<Group>();
                ArrayList<Child> ch_list;

                try {
                    Iterator<String> key = response.keys();
                    while (key.hasNext()) {
                        String k = key.next();

                        Group gru = new Group();
                        gru.setName(k);
                        ch_list = new ArrayList<Child>();

                        JSONArray ja = response.getJSONArray(k);

                        for (int i = 0; i < ja.length(); i++) {

                            JSONObject jo = ja.getJSONObject(i);

                            Child ch = new Child();
                            ch.setName(jo.getString("name"));
                            ch.setImage(jo.getString("image"));
                            ch.setId(jo.getString("id"));
                            ch_list.add(ch);
                        } // for loop end
                        gru.setItems(ch_list);
                        list.add(gru);
                    } // while loop end

                    ExpAdapter = new ExpandListAdapter(
                            MainActivity.this, list);
                    WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(ExpAdapter);
                    ExpandList.setAdapter(wrapperAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        cartController.getmInstance().addToRequesQueue(jsonObjReq, "jreq");
    }

    private void getInfoUser(final String devId) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Config.GET_USER_INFO + devId,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //parsing json
                        if (response.length() == 0) {
                            //Toasty.info(getApplication(), "Keranjang Belanja Anda Kosong..."+response, Toast.LENGTH_SHORT).show();
                            //countCart.setVisibility(View.INVISIBLE);
                        } else {
                            for (int x = 0; x < response.length(); x++) {
                                try {
                                    JSONObject obj = response.getJSONObject(x);
                                    namaUser.setText(obj.getString(Config.USER_NAME));
                                    emailUser.setText(obj.getString(Config.USER_MAIL));
                                    namaApartement.setText(obj.getString(Config.USER_APARTEMENT_NAME));
                                    towerApartement.setText("Tower " + obj.getString(Config.USER_APARTEMENT_TOWER).toUpperCase());
                                    lantaiApartement.setText("Lantai " + obj.getString(Config.USER_APARTEMENT_LANTAI).toUpperCase());
                                    nomorApartement.setText("No. " + obj.getString(Config.USER_APARTEMENT_NOMOR).toUpperCase());

                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ServerError) {
                    message = "Server tidak ditemukan...";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing data Error...";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut...";
                }
                Toasty.error(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    private void jumlahItem(final String devId) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(CEK_CART_URL + devId,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //parsing json
                        if (response.length() == 0) {
                            //Toasty.info(getApplication(), "Keranjang Belanja Anda Kosong..."+response, Toast.LENGTH_SHORT).show();
                            countCart.setVisibility(View.INVISIBLE);
                        } else {
                            for (int x = 0; x < response.length(); x++) {
                                try {
                                    JSONObject obj = response.getJSONObject(x);
                                    // Toast.makeText(MainActivity.this, obj.getString("jml"), Toast.LENGTH_SHORT).show();
                                    countCart.setVisibility(View.VISIBLE);
                                    countCart.setText(obj.getString("jml"));
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ServerError) {
                    message = "Server tidak ditemukan...";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing data Error...";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut...";
                }
                Toasty.error(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }


    public void getProductTerlaris() {
        // final ProgressDialog loading = ProgressDialog.show(this, "Please wait...","loading data...",false,false);

        jsonArrayRequest = new JsonArrayRequest(GET_TERLARIS_URL,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        swipe.setRefreshing(false);
                        swipe.setEnabled(false);
                        //    loading.dismiss();
                        banner.setVisibility(View.VISIBLE);
//                        terlaris.setVisibility(View.VISIBLE);
                        terbaru.setVisibility(View.VISIBLE);
//                        terlaris_JSONParser(response);

                        linear.removeView(layout);

                        final String device_id = Settings.Secure.getString(getApplication().getContentResolver(), Settings.Secure.ANDROID_ID);
                        // cekCartItem(device_id);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //loading.dismiss();
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Tidak ada Koneksi Internet..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();

                            //inject layout

                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);

                        } else if (volleyError instanceof ServerError) {
                            message = "Server tidak ditemukan..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();

                            //inject layout

                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);

                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Tidak ada Koneksi Internet..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);

                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing data error..";
                            // Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);

                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Tidak ada Koneksi Internet..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Koneksi TimeOut..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);
                        }
                        swipe.setRefreshing(false);
                    }

                });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    public void getAllCategory() {
        final ProgressDialog loading = ProgressDialog.show(this, "Please wait...", "loading data...", false, false);

        jsonArrayRequest = new JsonArrayRequest(GET_CATEGORY_URL,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        swipe.setRefreshing(false);
                        swipe.setEnabled(false);
                        loading.dismiss();
                        banner.setVisibility(View.VISIBLE);
//                        terlaris.setVisibility(View.VISIBLE);
                        terbaru.setVisibility(View.VISIBLE);
                        allcategory_JSONParser(response);

                        linear.removeView(layout);

                        final String device_id = Settings.Secure.getString(getApplication().getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        // cekCartItem(device_id);
                        jumlahItem(device_id);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Tidak ada Koneksi Internet..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();

                            //inject layout

                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);

                        } else if (volleyError instanceof ServerError) {
                            message = "Server tidak ditemukan..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();

                            //inject layout

                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);

                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Tidak ada Koneksi Internet..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);

                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing data error..";
                            // Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);

                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Tidak ada Koneksi Internet..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Koneksi TimeOut..";
                            //Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                            err_MSG.setText(message);
                            linear.removeView(layout);
                            linear.addView(layout);
                        }
                        swipe.setRefreshing(false);
                    }

                });

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);


    }

    public void allcategory_JSONParser(JSONArray array) {
        GetDataAdapter.clear();
        for (int i = 0; i < array.length(); i++) {

            GetDataAdapter GetDataAdapter2 = new GetDataAdapter();
            JSONObject json = null;
            try {
                json = array.getJSONObject(i);
                GetDataAdapter2.setProductName(json.getString(CATEGORY_PRODUCT_NAME));
                GetDataAdapter2.setProductImage(json.getString(CATEGORY_PRODUCT_IMG));
                GetDataAdapter2.setProductId(json.getString(CATEGORY_PRODUCT_ID));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // GetDataAdapter.add(null);
            GetDataAdapter.add(GetDataAdapter2);
        }
        recyclerViewadapter = new RVCategoryAdapter(GetDataAdapter, this);
        recyclerView.setAdapter(recyclerViewadapter);
    }


    public void terlaris_JSONParser(JSONArray array) {
        GetDataTerlarisAdapter.clear();
        for (int i = 0; i < array.length(); i++) {

            GetDataTerlarisAdapter GetDataTerlarisAdapter3 = new GetDataTerlarisAdapter();
            JSONObject json = null;
            try {
                json = array.getJSONObject(i);
                GetDataTerlarisAdapter3.setProductName(json.getString(TERLARIS_PRODUCT_NAME));
                GetDataTerlarisAdapter3.setProductImage(json.getString(TERLARIS_PRODUCT_IMG));
                GetDataTerlarisAdapter3.setProductId(json.getString(TERLARIS_PRODUCT_ID));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // GetDataAdapter.add(null);
            GetDataTerlarisAdapter.add(GetDataTerlarisAdapter3);
        }
        rvAdapterTerlaris = new RVCategoryTerlarisAdapter(GetDataTerlarisAdapter, this);
        //recyclerViewadapter = new RVCategoryTerlarisAdapter(GetDataTerlarisAdapter, this);
        rvTerlaris.setAdapter(rvAdapterTerlaris);
    }


    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            backto.setVisibility(View.INVISIBLE);
            judul.setText("diantar.in");
            judul.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
            getSupportFragmentManager().popBackStack();
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toasty.info(this, "Tekan sekali lagi untuk keluar...", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
            return;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.profil:
                fragment = Profile.newInstance();
                Bundle bundle = new Bundle();
                String nama = namaUser.getText().toString();
                String mail = emailUser.getText().toString();
                String apartemen = namaApartement.getText().toString();
                String towerT = towerApartement.getText().toString();
                String lantaiT = lantaiApartement.getText().toString();
                String nomorT = nomorApartement.getText().toString();
                bundle.putString("namaUsr", nama);
                bundle.putString("emailUsr", mail);
                bundle.putString("apartemen", apartemen);
                bundle.putString("tower", towerT);
                bundle.putString("lantai", lantaiT);
                bundle.putString("nomor", nomorT);
                fragment.setArguments(bundle);
                break;
            case R.id.riwayat:
                fragment = RiwayatOrder.newInstance();
                break;
            case R.id.bantuan:
                break;

        }
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment, "TAG_MENU").addToBackStack(null);
            ft.commit();
        }

        return super.onOptionsItemSelected(item);

    }

    private void signIn() {
        Intent sIn = new Intent(MainActivity.this, AkunActivity.class);
        startActivity(sIn);
    }


    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.profileSaya:
                fragment = Profile.newInstance();
                Bundle bundle = new Bundle();
                String nama = namaUser.getText().toString();
                String mail = emailUser.getText().toString();
                String apartemen = namaApartement.getText().toString();
                String towerT = towerApartement.getText().toString();
                String lantaiT = lantaiApartement.getText().toString();
                String nomorT = nomorApartement.getText().toString();
                bundle.putString("namaUsr", nama);
                bundle.putString("emailUsr", mail);
                bundle.putString("apartemen", apartemen);
                bundle.putString("tower", towerT);
                bundle.putString("lantai", lantaiT);
                bundle.putString("nomor", nomorT);
                fragment.setArguments(bundle);
                Log.d("Parameter yang di kirim", String.valueOf(bundle));
                break;
            case R.id.riwayatOrder:
                fragment = RiwayatOrder.newInstance();
                break;
            case R.id.contact:
                fragment = new Contact_Us();
                break;
            case R.id.about:
                fragment = new About_Us();
                break;
            default:
                // fragment = CategoryPage.newInstance();
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment, "TAG_MENU").addToBackStack(null);
            ft.commit();
        } else {
            Toast.makeText(this, "open activity", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(MainActivity.this, AkunActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        //calling the method displayselectedscreen and passing the id of selected menu
        displaySelectedScreen(item.getItemId());
        //make this method blank
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}
