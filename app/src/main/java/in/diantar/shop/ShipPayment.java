package in.diantar.shop;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

import in.diantar.shop.ExpedisiFragment.KurirKu;
import in.diantar.shop.MenuDrawer.About_Us;
import in.diantar.shop.viewKeranjang.ItemCart;
import in.diantar.shop.viewKeranjang.cartAdapter;

public class ShipPayment extends AppCompatActivity {

    private ProgressDialog dialog;
    private List<ItemCart> itemKeranjang = new ArrayList<ItemCart>();
    private cartAdapter adapter;
    private RadioGroup shipGroup;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ship_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarShip);
        setSupportActionBar(toolbar);
        setTitle("Shipping and Payment");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_avatar);
        }


        // dialog=new ProgressDialog(this);
        // dialog.setMessage("please wait...");
        // dialog.show();

        fragment = new KurirKu();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.expedisiLayout, fragment);
        ft.commit();

        shipGroup = (RadioGroup) findViewById(R.id.shipgrup);
        shipGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.kurirku:
                        fragment = new KurirKu();
                        break;
                    case R.id.expedisi:
                        fragment = new About_Us();
                        break;

                }
                if (fragment != null) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.expedisiLayout, fragment);
                    ft.commit();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        overridePendingTransition(R.anim.act_stay, R.anim.act_slidedown);
        return super.onOptionsItemSelected(item);
    }
}
