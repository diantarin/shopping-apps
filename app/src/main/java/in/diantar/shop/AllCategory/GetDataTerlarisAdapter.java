package in.diantar.shop.AllCategory;

/**
 * Created by MasToro on 30/03/2017.
 */

public class GetDataTerlarisAdapter {
    public String productImage;
    public String productName;
    public String productId;

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

}
