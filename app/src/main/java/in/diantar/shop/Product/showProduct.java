package in.diantar.shop.Product;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import in.diantar.shop.MainActivity;
import in.diantar.shop.R;
import in.diantar.shop.viewKeranjang.ItemCart;

import static java.lang.Integer.parseInt;

public class showProduct extends Fragment {

    List<GetProductAdapter> GetProductAdapter;

    private RecyclerView recyclerView;

    RecyclerView.Adapter recyclerViewProdukadapter;

    String JSON_PRODUCT_NAME = "name";
    String JSON_PRODUCT_IMG = "image";
    String JSON_PRODUCT_ID = "productid";
    String JSON_PRODUCT_PRICE = "harga";

    JsonArrayRequest jsonArrayRequest;

    RequestQueue requestQueue;
    private PopupWindow pwindo;
    private TextView judul;

    //listview cart item

    private ProgressDialog dialog;
    private List<ItemCart> itemKeranjang = new ArrayList<ItemCart>();
    private ImageButton btnfilter, backto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments

        View view = inflater.inflate(R.layout.main_product, container, false);//Inflate Layout
        TextView textIdp = (TextView) view.findViewById(R.id.idp);//Find textview Id
        TextView textNam = (TextView) view.findViewById(R.id.ctgName);
        TextView headerText = (TextView) view.findViewById(R.id.headerText);

        String categoryId = getArguments().getString("Category_ID");
        String categoryNm = getArguments().getString("Category_NAME");

        textIdp.setText(categoryId);
        headerText.setText(categoryNm);

        ((MainActivity) getActivity()).closeDrawer();
        return view;//return view

    }

    public static showProduct newInstance() {
        showProduct fragment = new showProduct();
        return fragment;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles

        // getActivity().setTitle("Product");

        GetProductAdapter = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.rvProduct);
        recyclerView.setHasFixedSize(true);

        TextView idCategory = (TextView) view.findViewById(R.id.idp);
        String categoryId = idCategory.getText().toString();

        judul = (TextView) getActivity().findViewById(R.id.titleToolbar);
        backto = (ImageButton) getActivity().findViewById(R.id.backto);

        backto.setVisibility(View.VISIBLE);
        getActivity().setTitle("");
        judul.setText("Product");

        backto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backto.setVisibility(View.INVISIBLE);
                judul.setText("diantar.in");
                judul.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
                getFragmentManager().popBackStack();
            }
        });

        // recyclerViewlayoutManager = new LinearLayoutManager(this);
        // recyclerView.setLayoutManager(recyclerViewlayoutManager);

        recyclerView.setLayoutManager(new GridLayoutManager(this.getActivity(), 2));
        getProductOfCategory(categoryId);

        //fab button
       /* FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fabCart);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(), "Open Cart", Toast.LENGTH_SHORT).show();
               // initiatePopupWindow(view);

                final String device_id = Settings.Secure.getString(getActivity().getContentResolver(),
                        Settings.Secure.ANDROID_ID);

                Intent cart = new Intent(getActivity(), KeranjangBelanja.class);
                cart.putExtra("device_id",device_id);
                startActivity(cart);

            }
        });*/

        //popup menu filter produk
     /*   btnfilter = (ImageButton)view.findViewById(R.id.filter);
        btnfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              //  Toast.makeText(getContext(),"Open", Toast.LENGTH_SHORT).show();
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(getActivity(), btnfilter);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.filter_popup, popup.getMenu());

                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if(item.getTitle()=="Urutkan Abjad"){

                        }else if(item.getTitle()=="Termurah"){

                        }

                        return true;
                    }
                });
            }
        });*/

    }


    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    public double getTotal(List<ItemCart> array) {

        int total = 0;
        for (int i = 0; i < array.size(); i++) {
            total = total + parseInt(array.get(i).getSubtotal());
        }
        //    Toast.makeText(activity, String.valueOf(total), Toast.LENGTH_SHORT).show();
        return total;
    }


    public void getProductOfCategory(String categoryId) {
        final ProgressDialog loading = ProgressDialog.show(this.getActivity(), "Wait...", "Fetching product...", false, false);

        jsonArrayRequest = new JsonArrayRequest("http://bungakasih.id:8080/webservices/getdata.jsp?q=PRODUCT&i=" + categoryId,
                // jsonArrayRequest = new JsonArrayRequest("http://bungakasih.id:8080/webservices/getdata.jsp?q=PRODUCT&i="+categoryId,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        loading.dismiss();
                        JSONParser(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Tidak ada koneksi Internet...";
                        } else if (volleyError instanceof ServerError) {
                            message = "Server tidak ditemukan...";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Tidak ada koneksi Internet...";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing data Error...";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut...";
                        }
                        Toasty.error(getContext(), message, Toast.LENGTH_LONG).show();
                        loading.dismiss();
                    }
                });

        requestQueue = Volley.newRequestQueue(this.getContext());

        requestQueue.add(jsonArrayRequest);
    }

    public void JSONParser(JSONArray array) {

        for (int i = 0; i < array.length(); i++) {

            GetProductAdapter GetProductAdapter2 = new GetProductAdapter();

            JSONObject json = null;
            try {

                json = array.getJSONObject(i);

                GetProductAdapter2.setProductName(json.getString(JSON_PRODUCT_NAME));
                GetProductAdapter2.setProductImage(json.getString(JSON_PRODUCT_IMG));
                GetProductAdapter2.setProductId(json.getString(JSON_PRODUCT_ID));
                GetProductAdapter2.setProductPrice(json.getString(JSON_PRODUCT_PRICE));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            GetProductAdapter.add(GetProductAdapter2);
        }
        recyclerViewProdukadapter = new RVProductAdapter(GetProductAdapter, getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(recyclerViewProdukadapter);
    }

}
